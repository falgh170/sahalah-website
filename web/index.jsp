<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>SAHALAH</title>


        <link href="bootstrap.min.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="style.css">

        <link href="font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="animate.min.css">

        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>


    </head>
    <body>


        <header class="navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="header_top">
                        <div class="col-md-2">
                            <div class="logo_img">
                                <a href="#"><img src="images/Logo-V1-01.png" alt="logoimage"></a>
                            </div>
                        </div>

                        <div class="col-md-10">
                            <div class="menu_bar">	
                                <nav role="navigation" class="navbar navbar-default">
                                    <div class="navbar-header">
                                        <button id="menu_slide"  aria-controls="navbar" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                    <div class="collapse navbar-collapse" id="navbar">

                                        <ul class="nav navbar-nav">
                                            <li><a href="#home" class="js-target-scroll">Home</a></li>
                                            <li><a href="#services" class="js-target-scroll">Log In</a></li>
                                            <li><a href="#portfolio" class="js-target-scroll">Sing In</a></li>


                                            <li><a href="#contact" class="js-target-scroll">Contact</a></li>

                                        </ul>      
                                    </div>



                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>

        <section id="home" class="top_banner_bg secondary-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top_banner">

                        </div>

                        <div class="col-md-6">
                            <div class="present">
                                <h4 style="color:#fff">Sahalah.. The fastest, safest and most flexible payment method for your Hajj journey</h4>

                                <h5> You cannot afford another day without Sahalah</h5>

                                <div class="section_btn">
                                    <a href="#"> <button class="btn" type="submit"> 
                                            App Store</button> </a>	
                                    <span>
                                        <a href="#"> 
                                            <button class="btn " type="submit">
                                                Play Store</button> </a> </span>							
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="present_img">
                                <img src="images/PHOTO-2018-08-02-17-50-26[1].jpg" alt="image">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>


        <section class="primary-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="section_heading">
                            <h2> AWESOME FEATURES </h2>

                            <h4> SAHALAH offers a wide range of features.. Start using Sahalah today for free!  .</h4>
                        </div>		

                        <div class="col-md-6">
                            <div class="features_detail">
                                <ul>
                                    <li>

                                        <i class="fa fa-user" aria-hidden="true"></i> 
                                        <h5>Refund</h5>
                                         Get refunded easily with Sahalah. s
                                    </li>

                                    <li>

                                        <i class="fa fa-paperclip" aria-hidden="true"></i> 
                                        <h5>Payment History</h5>
                                        Easy to Access your wallet and get history payment.
                                    </li>

                                    <li>

                                        <i class="fa fa-camera" aria-hidden="true"></i> 
                                        <h5> Recharge</h5>
                                        Recharge your wallet with many recharging channels .
                                    </li>

                                    <li>

                                        <i class="fa fa-desktop" aria-hidden="true"></i> 
                                        <h5> Transfer Money </h5>
                                        Easy to transfer money between wallets.
                                    </li>
                                </ul>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </section>


        <section id="services" class="padding_bottom_none our_service_bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section_heading section_heading_2">
                            <h2> Our Services </h2>

                            <h4> SAHALAH  providing a best services which help you to pay your hotel, food,Transportation, etc</h4>
                        </div>

                        <div class="col-md-5 pull-right">
                            <div class="services_detail">
                                <ul>
                                    <li>
                                        <a href="#"><span><i class="fa fa-html5" aria-hidden="true"></i></span> 
                                            <h5> Easy Pay </h5></a>
                                        <p> Just quick scan can pay your bills.</p>
                                    </li>

                                    <li>
                                        <a href="#"><span><i class="fa fa-desktop" aria-hidden="true"></i></span> 
                                            <h5> recharger </h5></a>
                                        <p> you can recharging your account from any stores or transfer from any bank </p>
                                    </li>

                                    <li>
                                        <a href="#"><span><i class="fa fa-rocket" aria-hidden="true"></i></span> 
                                            <h5> Unlimited Support </h5></a>
                                        <p> we will help you from any where.</p>
                                    </li>
                                </ul>

                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="services_img">
                                <img src="images/PHOTO-2018-08-02-17-50-27.jpg" alt="image-1">
                            </div>

                           
                        </div>

                    </div>
                </div>
            </div>
        </section>


        <section class="primary-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">


                        <section class="primary-bg">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="col-md-4">
                                            <div class="subscribe">
                                                <h3> Stay informed with our newsletter</h3>

                                                <h6> Subscribe to our email newsletter for useful tips and resources. </h6>

                                                <div class="subscribe_form">
                                                    <form>
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address" >
                                                        </div>
                                                    </form>

                                                    <div class="section_sub_btn">
                                                        <button class="btn btn-default" type="submit">  Subscribe </button>	
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                        <div class="col-md-4">
                                            <div class="workng_img">
                                                <img src="images/PHOTO-2018-08-02-17-50-26[1].jpg" alt="image">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="subscribe">
                                                <h3> Download App Now </h3>

                                                <h6> Select your device platform and get download started </h6>

                                                <div class="section_btn">
                                                    <button class="btn btn-default" type="submit"> <i class="fa fa-apple" aria-hidden="true"></i> App Store</button>	

                                                    <span><button class="btn btn-default" type="submit"><i class="fa fa-android" aria-hidden="true"></i> Play Store</button></span>							
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>


                        <footer class="third-bg">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="footer_top">
                                            <h4> Share SAHALAH With Your Friends  </h4>

                                            <ul>
                                                <li> <a href="http://facebook.com/"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                                                <li> <a href="http://twitter.com/"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                                                <li> <a href="http://linkedin.com/"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
                                                <li> <a href="http://google.com/"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                                                <li> <a href="http://youtu.be/"> <i class="fa fa-youtube-square" aria-hidden="true"></i> </a> </li>
                                                <li> <a href="https://www.instagram.com"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                                            </ul>
                                        </div>




                                    </div>
                                </div>
                            </div>

                            <div class="footer_bottom fourth-bg">
                                <!-- Keep Footer Credit Links Intact -->
                                <p> 2018 &copy; Copyright SAHALAH. All rights Reserved. </p>
                                <a href="#" class="backtop"> ^ </a>
                            </div>

                        </footer>
                        <script src="js/jquery.min.js"></script>
                        <script src="js/bootstrap.min.js"></script>
                        <script src="js/interface.js"></script> 
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#menu_slide").click(function () {
                                    $("#navbar").slideToggle('normal');
                                });
                            });
                        </script>
                        <!--Menu Js Right Menu-->
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#navbar > ul > li:has(ul)').addClass("has-sub");
                                $('#navbar > ul > li > a').click(function () {
                                    var checkElement = $(this).next();
                                    $('#navbar li').removeClass('dropdown');
                                    $(this).closest('li').addClass('dropdown');
                                    if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                                        $(this).closest('li').removeClass('dropdown');
                                        checkElement.slideUp('normal');
                                    }
                                    if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                                        $('#navbar ul ul:visible').slideUp('normal');
                                        checkElement.slideDown('normal');
                                    }
                                    if (checkElement.is('ul')) {
                                        return false;
                                    } else {
                                        return true;
                                    }
                                });
                            });
                        <!--end-->
                        </script>
                                <script type="text/javas                                cript">
    $("#navbar").on("clic                            k", func                            tion (event) {
        event.stopPropagation();
    });                                
    $(".dropdown-menu").on("clic                            k", func                            tion (event) {
        event.stopPropagation();                                
    });
    $(document).on("click", function (                            event) {
                            $(".dropdown-menu").slideUp('normal');
    });
                                
    $(".navbar-header").on("clic                            k", func                            tion (event) {
        event.stopPropagation();                                
    });
    $(document).on("click", fun                            ction (e                            vent) {
  $("                            #navbar").slideUp('n                            ormal');
 });
</script>

</body>
<!-- JS Plugins -->
  </html>
